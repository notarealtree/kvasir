package com.notarealtree.kvasir.resource;

import com.codahale.metrics.health.HealthCheck;
import com.notarealtree.kvasir.backing.BackingStoreHealthCheck;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StorageHealthCheck extends HealthCheck {
    private final BackingStoreHealthCheck delegate;

    @Inject
    public StorageHealthCheck(BackingStoreHealthCheck delegate) {
        this.delegate = delegate;
    }

    @Override
    protected Result check() throws Exception {
        return delegate.check();
    }
}
