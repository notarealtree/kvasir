package com.notarealtree.kvasir.resource;

import com.notarealtree.kvasir.backing.BackingStore;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Singleton
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/")
public class StorageResource {
    private final BackingStore backingStore;

    @Inject
    public StorageResource(BackingStore backingStore) {
        this.backingStore = backingStore;
    }

    @GET
    @Path("/key/{key}")
    public Object get(@PathParam("key") String key) {
        return backingStore.get(key);
    }

    @GET
    @Path("/prefix/{prefix}/key/{key}")
    public Object getWithPrefix(@PathParam("prefix") String prefix, @PathParam("key") String key) {
        return backingStore.get(prefix, key);
    }

    @GET
    public Collection<Object> getAll() {
        return backingStore.getAll();
    }

    @PUT
    @Path("/key/{key}")
    public void put(@PathParam("key") String key, Object value) {
        backingStore.put(key, value);
    }

    @PUT
    @Path("/prefix/{prefix}/key/{key}")
    public void putWithPrefix(@PathParam("prefix") String prefix, @PathParam("key") String key, Object value) {
        backingStore.put(prefix, key, value);
    }

    @DELETE
    @Path("/key/{key}")
    public void delete(@PathParam("key") String key) {
        backingStore.delete(key);
    }

    @DELETE
    @Path("/prefix/{prefix}/key/{key}")
    public void deleteWithPrefix(@PathParam("prefix") String prefix, @PathParam("key") String key) {
        backingStore.delete(prefix, key);
    }
}
