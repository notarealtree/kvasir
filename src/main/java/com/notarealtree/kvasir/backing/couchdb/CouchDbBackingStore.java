package com.notarealtree.kvasir.backing.couchdb;

import com.google.common.annotations.VisibleForTesting;
import com.notarealtree.kvasir.backing.BackingStore;
import com.notarealtree.kvasir.configuration.CouchDbConfiguration;
import org.ektorp.CouchDbConnector;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.Collection;
import java.util.stream.Collectors;

public class CouchDbBackingStore implements BackingStore {
    private final CouchDbConnector db;

    public CouchDbBackingStore(CouchDbConfiguration couchDbConfiguration) {
        HttpClient stdHttpClient = new StdHttpClient.Builder().host(couchDbConfiguration.getHost()).build();
        db = new StdCouchDbConnector("kvasir", new StdCouchDbInstance(stdHttpClient));
        db.createDatabaseIfNotExists();
    }

    @Override
    public Object get(String key) {
        return db.get(Object.class, concatenatePrefixAndKey(DEFAULT_PREFIX, key));
    }

    @Override
    public Object get(String prefix, String key) {
        return db.get(Object.class, concatenatePrefixAndKey(prefix, key));
    }

    @Override
    public Collection<Object> getAll() {
        return db.getAllDocIds().stream().map(docId -> db.get(Object.class, docId)).collect(Collectors.toSet());
    }

    @Override
    public void put(String key, Object value) {
        String keyWithPrefix = concatenatePrefixAndKey(DEFAULT_PREFIX, key);
        if (!db.contains(keyWithPrefix)) {
            db.create(keyWithPrefix, value);
            return;
        }
        db.create(keyWithPrefix, value);
    }

    @Override
    public void put(String prefix, String key, Object value) {
        String keyWithPrefix = concatenatePrefixAndKey(prefix, key);
        if (!db.contains(keyWithPrefix)) {
            db.create(keyWithPrefix, value);
            return;
        }
        db.create(keyWithPrefix, value);
    }

    @Override
    public void delete(String key) {
        String keyWithPrefix = concatenatePrefixAndKey(DEFAULT_PREFIX, key);
        String currentRevision = db.getCurrentRevision(keyWithPrefix);
        db.delete(keyWithPrefix, currentRevision);
    }

    @Override
    public void delete(String prefix, String key) {
        String keyWithPrefix = concatenatePrefixAndKey(prefix, key);
        String currentRevision = db.getCurrentRevision(keyWithPrefix);
        db.delete(keyWithPrefix, currentRevision);
    }

    @VisibleForTesting
    static String concatenatePrefixAndKey(String prefix, String key) {
        return String.format("%s:%s", prefix, key);
    }
}
