package com.notarealtree.kvasir.backing.couchdb;

import com.codahale.metrics.health.HealthCheck;
import com.notarealtree.kvasir.backing.BackingStoreHealthCheck;
import com.notarealtree.kvasir.configuration.CouchDbConfiguration;
import org.ektorp.CouchDbConnector;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.Objects;

public class CouchDbBackingStoreHealthCheck implements BackingStoreHealthCheck {
    private final CouchDbConnector db;

    public CouchDbBackingStoreHealthCheck(CouchDbConfiguration couchDbConfiguration) {
        HttpClient stdHttpClient = new StdHttpClient.Builder().host(couchDbConfiguration.getHost()).build();
        db = new StdCouchDbConnector("kvasir", new StdCouchDbInstance(stdHttpClient));
        db.createDatabaseIfNotExists();
    }

    @Override
    public HealthCheck.Result check() {
        if (!Objects.isNull(db.getDbInfo())) {
            return HealthCheck.Result.healthy();
        }
        return HealthCheck.Result.unhealthy("Could not reach couchdb");
    }
}
