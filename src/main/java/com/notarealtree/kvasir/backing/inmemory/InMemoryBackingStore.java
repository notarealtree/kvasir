package com.notarealtree.kvasir.backing.inmemory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.notarealtree.kvasir.backing.BackingStore;

import java.util.Collection;
import java.util.List;

public class InMemoryBackingStore implements BackingStore {
    @VisibleForTesting
    final Table<String, String, Object> storage;

    public InMemoryBackingStore() {
        this.storage = HashBasedTable.create();
    }

    @Override
    public Object get(String key) {
        return storage.get(DEFAULT_PREFIX, key);
    }

    @Override
    public Object get(String prefix, String key) {
        validatePrefix(prefix);
        return storage.get(prefix, key);
    }

    @Override
    public List<Object> getAll() {
        return Lists.newLinkedList(storage.values());
    }

    @Override
    public void put(String key, Object value) {
        storage.put(DEFAULT_PREFIX, key, value);
    }

    @Override
    public void put(String prefix, String key, Object value) {
        validatePrefix(prefix);
        storage.put(prefix, key, value);
    }

    @Override
    public void delete(String key) {
        storage.remove(DEFAULT_PREFIX, key);
    }

    @Override
    public void delete(String prefix, String key) {
        validatePrefix(prefix);
        storage.remove(prefix, key);
    }
}
