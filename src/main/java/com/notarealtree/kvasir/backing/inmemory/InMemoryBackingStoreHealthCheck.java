package com.notarealtree.kvasir.backing.inmemory;

import com.codahale.metrics.health.HealthCheck;
import com.notarealtree.kvasir.backing.BackingStoreHealthCheck;

public class InMemoryBackingStoreHealthCheck implements BackingStoreHealthCheck {
    @Override
    public HealthCheck.Result check() {
        return HealthCheck.Result.healthy();
    }
}
