package com.notarealtree.kvasir.backing.redis;

import com.codahale.metrics.health.HealthCheck;
import com.notarealtree.kvasir.backing.BackingStoreHealthCheck;
import redis.clients.jedis.Jedis;

public class RedisBackingStoreHealthCheck implements BackingStoreHealthCheck {
    private final Jedis jedis;

    public RedisBackingStoreHealthCheck(Jedis jedis) {
        this.jedis = jedis;
    }

    @Override
    public HealthCheck.Result check() {
        return jedis.clusterInfo() != null
                ? HealthCheck.Result.healthy() : HealthCheck.Result.unhealthy("Redis could not be reached");
    }
}
