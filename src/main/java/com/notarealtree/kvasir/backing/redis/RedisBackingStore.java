package com.notarealtree.kvasir.backing.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.notarealtree.kvasir.backing.BackingStore;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanResult;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class RedisBackingStore implements BackingStore {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final Jedis jedis;

    public RedisBackingStore(Jedis jedis) {
        this.jedis = jedis;
    }

    @Override
    public Object get(String key) {
        String objectAsString = jedis.get(concatenatePrefixAndKey(DEFAULT_PREFIX, key));
        return deserializeObject(objectAsString);
    }

    @Override
    public Object get(String prefix, String key) {
        String objectAsString = jedis.get(concatenatePrefixAndKey(prefix, key));
        return deserializeObject(objectAsString);
    }

    @Override
    public List<Object> getAll() {
        ScanResult<String> scan = jedis.scan("0");
        return scan.getResult().stream().map(RedisBackingStore::deserializeObject).collect(Collectors.toList());
    }

    @Override
    public void put(String key, Object value) {
        jedis.set(concatenatePrefixAndKey(DEFAULT_PREFIX, key), serializeObject(value));
    }

    @Override
    public void put(String prefix, String key, Object value) {
        jedis.set(concatenatePrefixAndKey(prefix, key), serializeObject(value));
    }

    @Override
    public void delete(String key) {
        jedis.del(concatenatePrefixAndKey(DEFAULT_PREFIX, key));
    }

    @Override
    public void delete(String prefix, String key) {
        jedis.del(concatenatePrefixAndKey(prefix, key));
    }

    @VisibleForTesting
    static String concatenatePrefixAndKey(String prefix, String key) {
        return String.format("%s:%s", prefix, key);
    }

    private static Object deserializeObject(String objectAsString) {
        try {
            return objectMapper.readValue(objectAsString, Object.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String serializeObject(Object value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
