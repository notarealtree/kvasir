package com.notarealtree.kvasir.backing;

import com.codahale.metrics.health.HealthCheck;

public interface BackingStoreHealthCheck {
    HealthCheck.Result check();
}
