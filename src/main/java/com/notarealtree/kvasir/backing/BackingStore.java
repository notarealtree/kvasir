package com.notarealtree.kvasir.backing;

import com.google.common.base.Preconditions;

import java.util.Collection;

public interface BackingStore {
    String DEFAULT_PREFIX = "_default";

    /**
     * Get Object, will return null if not present
     */
    Object get(String key);

    /**
     * Get Object stored using prefix, will return null if not present
     */
    Object get(String prefix, String key);

    Collection<Object> getAll();

    /**
     * Put new Object. Will override previous values put
     */
    void put(String key, Object value);

    /**
     * Put new Object with Prefix. Will override previously put values.
     */
    void put(String prefix, String key, Object value);

    /**
     * Delete Object with the given key.
     */
    void delete(String key);

    /**
     * Delete Object with the given prefix and key.
     */
    void delete(String prefix, String key);

    default void validatePrefix(String prefix) {
        Preconditions.checkArgument(
                !prefix.startsWith("_"),
                "Illegal use of prefix beginning with _ (%s). These are internally reserved.",
                prefix);
    }

    enum Implementation {
        IN_MEMORY("InMemory"),
        REDIS("Redis"),
        COUCH_DB("CouchDb");
        private final String name;

        Implementation(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
