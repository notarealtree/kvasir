package com.notarealtree.kvasir.configuration;

import com.notarealtree.kvasir.backing.BackingStore;
import io.dropwizard.Configuration;

import java.util.Optional;

public class KvasirConfiguration extends Configuration {
    public BackingStore.Implementation backingStore;
    public Optional<RedisConfiguration> redis;
    public Optional<CouchDbConfiguration> couchDb;
}
