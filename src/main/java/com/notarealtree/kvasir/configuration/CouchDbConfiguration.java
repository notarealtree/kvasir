package com.notarealtree.kvasir.configuration;

public class CouchDbConfiguration {
    private String host;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
