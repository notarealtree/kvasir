package com.notarealtree.kvasir.configuration;

import java.util.Optional;

public class RedisConfiguration {
    private String host;
    private Optional<Integer> port;
    private Optional<Boolean> ssl;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Optional<Integer> getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = Optional.ofNullable(port);
    }

    public Optional<Boolean> getSsl() {
        return ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = Optional.ofNullable(ssl);
    }
}
