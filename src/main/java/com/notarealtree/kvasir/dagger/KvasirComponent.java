package com.notarealtree.kvasir.dagger;

import com.notarealtree.kvasir.resource.StorageHealthCheck;
import com.notarealtree.kvasir.resource.StorageResource;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = KvasirModule.class)
public interface KvasirComponent {
    StorageResource storageResource();
    StorageHealthCheck storageHealthCheck();
}
