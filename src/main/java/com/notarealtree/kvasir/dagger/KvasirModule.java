package com.notarealtree.kvasir.dagger;

import com.google.common.base.Preconditions;
import com.notarealtree.kvasir.backing.BackingStore;
import com.notarealtree.kvasir.backing.BackingStoreHealthCheck;
import com.notarealtree.kvasir.backing.couchdb.CouchDbBackingStore;
import com.notarealtree.kvasir.backing.inmemory.InMemoryBackingStore;
import com.notarealtree.kvasir.backing.inmemory.InMemoryBackingStoreHealthCheck;
import com.notarealtree.kvasir.backing.redis.RedisBackingStore;
import com.notarealtree.kvasir.backing.redis.RedisBackingStoreHealthCheck;
import com.notarealtree.kvasir.configuration.KvasirConfiguration;
import com.notarealtree.kvasir.configuration.RedisConfiguration;
import dagger.Module;
import dagger.Provides;
import redis.clients.jedis.Jedis;

@Module
public class KvasirModule {
    private KvasirConfiguration configuration;

    public KvasirModule (KvasirConfiguration configuration) {
        this.configuration = configuration;
    }

    @Provides
    public BackingStore backingStore() {
        switch (configuration.backingStore) {
            case REDIS:
                Preconditions.checkArgument(
                        configuration.redis.isPresent(),
                        "Backing Store %s selected but no Redis config was supplied.",
                        BackingStore.Implementation.REDIS);
                return new RedisBackingStore(jedis());
            case COUCH_DB:
                Preconditions.checkArgument(
                        configuration.couchDb.isPresent(),
                        "Backing Store %s selected but no config was supplied.",
                        BackingStore.Implementation.COUCH_DB);
                return new CouchDbBackingStore(configuration.couchDb.get());
            case IN_MEMORY:
                return new InMemoryBackingStore();
            default:
                throw new IllegalArgumentException(
                        String.format("Backing store type %s is not supported.", configuration.backingStore));
        }
    }

    @Provides
    public BackingStoreHealthCheck backingStoreHealthCheck() {
        switch (configuration.backingStore) {
            case REDIS:
                return new RedisBackingStoreHealthCheck(jedis());
            case IN_MEMORY:
                return new InMemoryBackingStoreHealthCheck();
            default:
                throw new IllegalArgumentException(
                        String.format("Backing store type %s is not supported.", configuration.backingStore));
        }
    }

    private Jedis jedis() {
        RedisConfiguration redisConfig = configuration.redis.get();
        int port = redisConfig.getPort() == null ? 6379 : redisConfig.getPort().get();
        boolean ssl = redisConfig.getSsl() != null;
        return new Jedis(redisConfig.getHost(), port, ssl);
    }
}
