package com.notarealtree.kvasir;

import com.notarealtree.kvasir.configuration.KvasirConfiguration;
import com.notarealtree.kvasir.dagger.DaggerKvasirComponent;
import com.notarealtree.kvasir.dagger.KvasirComponent;
import com.notarealtree.kvasir.dagger.KvasirModule;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class KvasirServer extends Application<KvasirConfiguration> {
    private static final Logger log = LoggerFactory.getLogger(KvasirServer.class);

    @Override
    public void run(KvasirConfiguration configuration, Environment environment) throws Exception {
        KvasirComponent kvasirComponent = DaggerKvasirComponent.builder()
                .kvasirModule(new KvasirModule(configuration))
                .build();
        environment.jersey().register(kvasirComponent.storageResource());
        environment.healthChecks().register("BackingStoreHealthCheck", kvasirComponent.storageHealthCheck());
        log.info(String.format("Configured backing store: %s", configuration.backingStore.getName()));
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            args = new String[]{"server", "config.yml"};
        }
        new KvasirServer().run(args);
    }
}
