package com.notarealtree.kvasir.backing.redis;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.notarealtree.kvasir.backing.BackingStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanResult;

import java.util.List;

import static com.notarealtree.kvasir.backing.redis.RedisBackingStore.concatenatePrefixAndKey;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RedisBackingStoreTest {
    private static final String PREFIX = "prefix";
    private static final String DEFAULT_PREFIX = BackingStore.DEFAULT_PREFIX;
    private static final String KEY = "key";
    private static final Object VALUE = ImmutableMap.of("foo", "bar");
    private static final String VALUE_JSON = "{\"foo\":\"bar\"}";

    @Mock
    private Jedis jedis;

    private RedisBackingStore store;

    @Before
    public void before() {
        store = new RedisBackingStore(jedis);
    }


    @Test
    public void getWithKeyReturnsValue() {
        when(jedis.get(concatenatePrefixAndKey(DEFAULT_PREFIX, KEY))).thenReturn(VALUE_JSON);

        assertThat(store.get(KEY)).isEqualTo(VALUE);
    }

    @Test
    public void getWithPrefixAndKeyReturnsValue() {
        when(jedis.get(concatenatePrefixAndKey(PREFIX, KEY))).thenReturn(VALUE_JSON);

        assertThat(store.get(PREFIX, KEY)).isEqualTo(VALUE);
    }

    @Test
    public void getAllReturnsAll() {
        ScanResult<String> result = mock(ScanResult.class);
        List<String> values = ImmutableList.of("{\"foo\": \"bar\"}", "\"test\"");
        when(result.getResult()).thenReturn(values);
        when(jedis.scan("0")).thenReturn(result);

        assertThat(store.getAll()).isEqualTo(ImmutableList.of(VALUE, "test"));
    }

    @Test
    public void putStoresValue() {
        store.put(KEY, VALUE);

        verify(jedis).set(concatenatePrefixAndKey(DEFAULT_PREFIX, KEY), VALUE_JSON);
    }

    @Test
    public void putWithPrefixStoresValue() {
        store.put(PREFIX, KEY, VALUE);

        verify(jedis).set(concatenatePrefixAndKey(PREFIX, KEY), VALUE_JSON);
    }

    @Test
    public void deleteWithKeyDeletesValue() {
        store.delete(KEY);

        verify(jedis).del(concatenatePrefixAndKey(DEFAULT_PREFIX, KEY));
    }

    @Test
    public void deleteWithPrefixKeyDeletesValue() {
        store.delete(PREFIX, KEY);

        verify(jedis).del(concatenatePrefixAndKey(PREFIX, KEY));
    }
}
