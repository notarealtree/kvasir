package com.notarealtree.kvasir.backing.inmemory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class InMemoryBackingStoreTest {
    private static final String PREFIX = "prefix";
    private static final String DEFAULT_PREFIX = "_default";
    private static final String KEY = "key";
    private static final Object VALUE = ImmutableMap.of("foo", "bar");

    private InMemoryBackingStore store;

    @Before
    public void before() {
        store = new InMemoryBackingStore();
    }

    @Test
    public void getWithKeyReturnsValue() {
        store.storage.put("_default", KEY, VALUE);

        assertThat(store.get(KEY)).isEqualTo(VALUE);
    }

    @Test
    public void getWithPrefixAndKeyReturnsValue() {
        store.storage.put(PREFIX, KEY, VALUE);

        assertThat(store.get(PREFIX, KEY)).isEqualTo(VALUE);
    }

    @Test
    public void getAllReturnsAll() {
        store.storage.put(PREFIX, KEY, VALUE);
        store.storage.put("foo", "bar", "test");

        assertThat(store.getAll()).isEqualTo(ImmutableList.of(VALUE, "test"));
    }

    @Test
    public void getWithPrefixThrowsIfUsingDefaultPrefix() {
        assertThatThrownBy(() -> store.get(DEFAULT_PREFIX, KEY))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Illegal use of prefix beginning with _ (_default). These are internally reserved.");
    }

    @Test
    public void putStoresValue() {
        store.put(KEY, VALUE);

        assertThat(store.storage.get(DEFAULT_PREFIX, KEY)).isEqualTo(VALUE);
    }

    @Test
    public void putWithPrefixStoresValue() {
        store.put(PREFIX, KEY, VALUE);

        assertThat(store.storage.get(PREFIX, KEY)).isEqualTo(VALUE);
    }

    @Test
    public void putWithIllegalPrefixThrows() {
        assertThatThrownBy(() -> store.put(DEFAULT_PREFIX, KEY, VALUE))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Illegal use of prefix beginning with _ (_default). These are internally reserved.");
    }

    @Test
    public void deleteWithKeyDeletesValue() {
        store.storage.put("_default", KEY, VALUE);
        store.delete(KEY);

        assertThat(store.storage.contains(DEFAULT_PREFIX, KEY)).isFalse();
    }

    @Test
    public void deleteWithPrefixKeyDeletesValue() {
        store.storage.put(PREFIX, KEY, VALUE);
        store.delete(PREFIX, KEY);

        assertThat(store.storage.contains(PREFIX, KEY)).isFalse();
    }

    @Test
    public void deleteWithIllegalPrefixThrows() {
        assertThatThrownBy(() -> store.delete(DEFAULT_PREFIX, KEY))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Illegal use of prefix beginning with _ (_default). These are internally reserved.");
    }
}
