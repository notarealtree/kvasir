FROM openjdk:8-jre-alpine

EXPOSE 8080 8081

ADD build/libs/kvasir-server-*.jar kvasir-server.jar

CMD java -jar kvasir-server.jar
